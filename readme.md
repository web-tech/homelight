# About
Homelight is a project to utilise your Pi and have fun (or to show off to your friends). You can switch radio-controlled (433MHz) bulbs and sockets (or any other devices) from your phone without relying on any external provider like Amazon or Google.
Homelight ports other RF control projects to Django.

### Known issues
- The setup shown below is working, however, it doesn't mean any guarantee that it will work with any other similar parts.
- The radio transmitter is directly controlled via the Pi's GPIO port, therefore, any transferred signal can be broken based on the current CPU usage. (Running a Django server without any incoming request can easily use the 40-60% of the CPU on a Raspberry Pi Zero w)
- There is a great chance the cheap RF controllers will use one of the microchips listed in the recommended devices [here](https://pypi.org/project/rpi-rf/ "here"), but never get disappointed if it's not working or it takes days of trying to record the right code from the controller.

### Kudos
I would never start to dig deeper into the magical world of RF control if my brother doesn't get me a Pi, so thanks.
And this project couldn't be here without the hard work of:
- Micha LaQua (milaq) - https://github.com/milaq/rpi-rf
- Suat Özgür (sui77) - https://github.com/sui77/rc-switch
- And many others who inspired them, us, you.

# Set up
I've used the same set up as you find it on milaq's [project](https://github.com/milaq/rpi-rf/blob/master/README.rst#wiring-diagram-example "project").

# Usage
#### Content
1. Install Rasbian
2. Install Python (if not available)
3. Get git
4. Download and configure Homelight
5. Onboot
6. Start


#### 1. INSTALL RASBIAN
Before you [install a new OS](https://www.raspberrypi.org/documentation/installation/installing-images/README.md "install a new OS") on your SD card make sure you have formatted it to FAT filesystem.
You can use a tool like [this](https://www.sdcard.org/downloads/formatter/ "this").

Download an official Raspbian system https://www.raspberrypi.org/downloads/
To flash it to the SD card you can use [Etcher](https://www.balena.io/etcher/ "Etcher").

> Note
If I need access to the Pi, I'm using SSH connection/Terminal only, but feel free to use any desktop distribution as well, the whole project should work just perfect on that version as well.

After the system has been installed, don't forget the below:
`sudo raspi-config`
- Set up your wireless connection (or just connect via ethernet)
(2-Network Options -> N2 Wi-fi)
After it's done by using `curl` you can verify whether the connection has been set up properly. i.e.
`curl -I https://www.bbc.co.uk`
- Enable SSH
(5-Interfacing Options -> P2 SSH -> yes)

- Get the Pi's IP address
You probably won't use SSH, but you will need it to use your switches.
`hostname -I`
>Note
You'll need to add your device to the [known hosts](https://serverfault.com/questions/321167/add-correct-host-key-in-known-hosts-multiple-ssh-host-keys-per-hostname "known hosts") if you want to connect via SSH.

- Update your system
`sudo apt-get update`


#### 2. INSTALL PYTHON
You can use `venv`, but it isn't necessary. I've installed on the computer as is.
Well, my Rasbian distribution came with preinstalled Python 2 and Python 3, so that's great. However, if not you can easily install it:
`sudo apt-get install python3.6`
Make sure Python 3 pip is installed
`sudo apt-get install pyton3-pip`

#### 3. GET GIT
If git isn't preinstalled you will need the command below:
`sudo apt-get install git`
In case you get any error messages, follow the recommendations and instructions.

#### 4. DOWNLOAD AND CONFIGURE HOMELIGHT 
Once you have git, run the command below. *(Note, I've installed it into my user root directory but feel free to download it into any other folder. You will need the right path in the following steps.)*
`git clone https://gitlab.com/web-tech/homelight/`

Install all dependencies from requirements.txt file
`pip3 install -r homelight/requirements.txt`

###### Configuration
Before you can use the tool make sure everything is ready. Run the below:
`python3 manage.py collectstatic` -> to move all static files (images and CSS) into a folder where from Django can serve them
`python3 manage.py migrate` -> to create an sqlite database and to make it's up to date if a new version of the project was installed
`python3 manage.py createsuperuser` -> to create your admin user to Django

###### Move/download transmitter script
*You don't have to follow the diagram above on the setup, however, make sure the send.py file below will operate with the right GPIO pin. You can modify the default GPIO pin in the file.*

- In the package you already have the send.py file, you can move it to the user root folder:
`mv homelight/send.py .`
- Or you can download it from [milaq](https://github.com/milaq/rpi-rf "milaq")'s git.
`wget -O send.py https://raw.githubusercontent.com/milaq/rpi-rf/master/scripts/rpi-rf_send`

#### 5. ONBOOT
There are many options how you can make your Pi automatically start Django when [the system is started](https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/ "the system is started"). I'm using [Crontab](https://www.dexterindustries.com/howto/auto-run-python-programs-on-the-raspberry-pi/ "Crontab") solution as rc.local couldn't call Python.
1. Open crontab and add a new line to the list.
If this is the first time you start crontab, choose your preferred editor or go with the recommended one.
    1. `crontab -e`
    2. And add a line:
        ```shell
        @reboot python3 /home/pi/homelight/manage.py runserver 0.0.0.0:8080 &
        ```
2. Save the file:
    1. `ctrl`+`x`
    2. `y`
    3. `enter`

#### 6. START
Now you can reboot your Pi
`sudo reboot`

Into your browser (mobile or desktop) enter your Pi's IP address and add `:8080/lights/index`
Example:
`192.168.1.122:8080/lights/index`

The site will offer you to enter into admin site and add your codes to the database. If that's done, there's nothing left but enjoy your lights.

> Note
You can add this page to your home screen on your mobile device.
