from django.db import models

# Create your models here.
class Switch(models.Model):
    #Name will appear on button, while value is the code that needs to be sent via RF
    switch_name = models.CharField(max_length=200)
    switch_value = models.CharField(max_length=10)
    switch_pulselength = models.CharField(max_length=10)

    class Meta:
        verbose_name_plural = "switches"
