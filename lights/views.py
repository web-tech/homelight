from django.shortcuts import render, redirect
from django.http import QueryDict
import subprocess
from .models import Switch


def index(request):
    switches = Switch.objects.order_by('switch_name')
    context = {'switches': switches}

    # Store switch name and code & pulse length value pairs to use it later
    switch_dictionary = {}
    for switch in switches:
        switch_dictionary[switch.switch_name] = {"switch_code":switch.switch_value, "switch_pulselength":switch.switch_pulselength}

    # Normal page load request
    if request.method == 'GET':
        return render(request, 'index.html', context)

    # Based on selected switch, the code to be sent via external script.
    elif request.method == 'POST':
        # Using Switch dictionary the requested code and pulse length can be find and applied
        code = switch_dictionary[QueryDict(request.body)['Switch']]["switch_code"]
        pulse_length = switch_dictionary[QueryDict(request.body)['Switch']]["switch_pulselength"]
        subprocess.Popen(["python3", "/home/pi/send.py", "-p", pulse_length, "-t", "1", "-r", "30", code])
        return redirect("index", permanent="true")
